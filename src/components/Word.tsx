'use client';

import { Tooltip } from 'react-tooltip';
import 'react-tooltip/dist/react-tooltip.css';

interface WordProps {
    word: string,
    definition: string
}

export default function Word(props: WordProps) {
    return (
    <span className="p-1" data-tooltip-id="my-tooltip" data-tooltip-content={props.definition} data-tooltip-place="top">
        {props.word}
        <Tooltip openOnClick id="my-tooltip" />
    </span>
    );
}