'use client';

import { useState, useEffect } from 'react';

import Word from '@/components/Word';

export default function Home() {

  const [text, setText] = useState("");

  useEffect(() => {
    setText("In den Tiefen des alten Waldes, verborgen unter einem Baldachin aus immergrünen Blättern, liegt ein kleiner, kristallklarer See. Sein Wasser ist so rein, dass es fast unsichtbar erscheint, und in seiner Stille spiegeln sich die Geheimnisse vergangener Zeiten. Legenden umweben diesen Ort; es heißt, dass hier einst eine antike Zivilisation lebte, deren Weisheit so tief war wie die Wurzeln der umstehenden Bäume. Wanderer, die durch diese Gegend streifen, berichten oft von einem Gefühl tiefer Ruhe und einer fast greifbaren Präsenz, die in der Luft liegt, als ob die Geister der Vergangenheit noch immer ihre Geschichten flüstern würden.");
  });

  return (
    <div>
      <h1>Hello, NextJS!</h1>
        {text.split(' ').map((word, index) => <Word key={index} word={word} definition="not yet defined" />)}
    </div>
  
  );


}
